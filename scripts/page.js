if (typeof $ === "undefined") {
    $ = {};
}

{
    // The system sometimes needs a temporary hash to navigate to. This
    // needs to be handled specially (e.g., the user should not be able
    // to navigate back to this hash, and navigating it should not trigger
    // callbacks).
	const TEMPORARY_HASH = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER).toString(36);

    $.getHash = function() {
        return window.location.hash ? window.location.hash.substring(1) : "";
    }

    $.setHash = function(hash) {
        window.location.hash = hash ? `#${hash}` : "";
    }

    // If you add an <article> to the page, the CSS which is meant to show it
    // might not be applied properly (this appears to be a Google Chrome bug).
    // This function is provided to force an update of the CSS :target selector,
    // as a workaround.
    $._refreshHash = function() {
        const hash = $.getHash();
        if (hash) {
            $.setHash(TEMPORARY_HASH);
            $.setHash(hash);
        }
    }

    // This function navigates back, but unfortunately it tends to crash Google Chrome
    // due to a defect (https://bugs.chromium.org/p/chromium/issues/detail?id=1038223).
    // As a result, I've kept it unused for now.
    $._back = function() {
        if (window.history.length > 2) {
            do {
                window.history.back();
            } while ($.getHash() == TEMPORARY_HASH);
        }
    }

    const updateCallbacks = [];
    $.onUpdate = function(callback) {
        updateCallbacks.push(callback);
    }

    $.updateState = function(data) {
        const newState = { ...$.getState(), ...data };
        $._setState(newState);
        update();
    }

    $.toggleState = function(fieldName) {
        const value = Boolean($.getState()[fieldName]);
        const newValue = !value;
        $.updateState({[fieldName]: newValue});
        return newValue;
    }

    function update() {
        const hash = $.getHash();
        updateCallbacks.forEach(callback => callback(hash, $.getState()));
    }

    window.addEventListener("hashchange", () => {
        if ($.getHash() != TEMPORARY_HASH) {
            update();
        }
    });

    function loadModule(url) {
        return new Promise((resolve, _) => {
            const script = document.createElement("script");
            script.setAttribute("src", url);
            script.addEventListener("load", e => {
                e.target.remove();
                resolve();
            });
            document.body.appendChild(script);
        });
    }

    const loadOperations = [
        () => loadModule("../../configuration.js"),
        () => loadModule("../../scripts/page/constants.js"),
        () => loadModule("../../scripts/shared/messaging.js"),
        () => loadModule("../../scripts/shared/state.js"),
        () => $._loadState(),
        () => loadModule("../../scripts/shared/saving.js"),
        () => loadModule("../../scripts/shared/audio.js"),
        () => loadModule("../../scripts/page/api.js"),
        () => loadModule("../../scripts/page/loadControls.js"),
        () => loadModule("../../scripts/page/moveAndResize.js"),
        () => loadModule("../../scripts/page/editor.js"),
        () => loadModule("../../scripts/page/editorSetup.js"),
    ];
    
    // Loads all modules and then calls callbacks
    loadOperations.reduce(
        (accumulator, loadOperation) => accumulator.then(loadOperation),
        Promise.resolve()
    ).then(update);

    $.onLoad = function(callback) {
        loaded.then(callback);
    }
}
