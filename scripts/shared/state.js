{
    let state = {};

    const SESSION_STORAGE_ERROR = "Could access sessionStorage. "
        + "This is only used when accessing pages directly from the filesystem.";

    $.getState = function() {
        return state;
    }
    
    $._setState = function(newState) {
        state = newState;
        if ($.IS_CHILD) {
            $._commandParent("_setState", state);
        } else {
            try {
                window.sessionStorage.setItem("state", JSON.stringify(state));
            } catch (e) {
                console.info(SESSION_STORAGE_ERROR, e);
            }
        }
    }

    $._loadState = function() {
        return new Promise((resolve, reject) => {
            try {
                if ($.IS_CHILD) {
                    $._onCommand("receiveState", newState => {
                        state = newState;
                        resolve();
                    });
                    $._commandParent("sendState");
                    setTimeout(() => {
                        reject("Request state from parent timed out.");
                    }, 1000);
                } else {
                    let stateJson = "{}";
                    try {
                        stateJson = window.sessionStorage.getItem("state") || stateJson;
                    } catch (e) {
                        console.info(SESSION_STORAGE_ERROR, e);
                    }
                    state = JSON.parse(stateJson);
                    resolve();
                }
            } catch (e) {
                reject(`Could not load state. ${e}`);
            }
        }).catch(console.error)
    };
    
    $._onCommand("_setState", $._setState);
    $._onCommand("sendState", () => {
        $._commandChild("child", "receiveState", state);
    });
}
