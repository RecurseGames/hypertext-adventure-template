{
	let pageModified = false;
	window.onbeforeunload = function(e) {
		if (pageModified) {
			e.preventDefault();
			return "Page has not been saved, are you sure you want to close it?";
		} else {
			// For browser compatibility
			return undefined;
		}
	};

	$._setPageModified = function() {
		pageModified = true;
	}

	$._exportHtml = async function(html, filename) {
		if ($.IS_CHILD) {
			$._commandParent("_exportHtml", {html, filename});
		} else {
			var content = new Blob(
				[html],
				{type: "text/html"},
			);
			const fileHandle = await window.showSaveFilePicker({
				suggestedName: filename,
			});
			const fileStream = await fileHandle.createWritable();
			await fileStream.write(content);
			await fileStream.close();
		}
	}
	
	$._onCommand("_exportHtml", async commandData => {
        const { html, filename } = commandData;
		$._exportHtml(html, filename);
    });

    const beforeSaveCallbacks = [];
    $._beforeSave = function(callback) {
        beforeSaveCallbacks.push(callback);
    }

	const afterSaveCallbacks = [];
    $._afterSave = function(callback) {
        afterSaveCallbacks.push(callback);
    }

	$._save = async function() {
		// Get page into saveable state
		document.title = $.PAGE_NAME;
		$.updateState({"_isSaving": true});
        beforeSaveCallbacks.forEach(callback => callback());

		// Remove empty notes
		const notes = document.getElementsByTagName("article");
		for (let note of notes) {
			if (!note.innerHTML) {
				note.remove();
			}
		}
		
		// Save the page
		const formattedHTML = document.documentElement.outerHTML.replaceAll(
			"</article>", "</article>\n"
		).replaceAll(
			"\n\n", "\n"
		);
		$._exportHtml(`<!DOCTYPE html>${formattedHTML}`, $.PAGE_NAME);

		// Restore
		$.updateState({"_isSaving": false});
        afterSaveCallbacks.forEach(callback => callback());

		pageModified = false;
	}
}
