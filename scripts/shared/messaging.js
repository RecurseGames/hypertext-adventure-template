if (typeof $ === "undefined") {
    $ = {};
}

{
    $.IS_CHILD = window !== top;

    const commandHandlers = {};
    window.addEventListener('message', async function(event) {
        const { data } = event;
        const { command, commandData } = data;
        const handlers = commandHandlers[command];
        if (handlers) {
            handlers.forEach(handler => handler(commandData));
        }
    });

    $._onCommand = function(command, handler) {
        let handlers = commandHandlers[command];
        if (!handlers) {
            handlers = [];
            commandHandlers[command] = handlers;
        }
        handlers.push(handler);
    }

    $._commandChild = function(id, command, commandData) {
        const child = document.getElementById(id);
        if (child) {
            child.contentWindow.postMessage({ command, commandData }, "*");
        } else {
            console.error(`Command ${command} failed: Child "${id}" not found.`);
        }
    }

    $._hasParent = function() {
        return window !== top;
    }

    $._commandParent = function(command, commandData) {
        if (window !== top) {
            window.parent.postMessage({ command, commandData }, "*");
            return true;
        } else {
            console.error(`Command ${command} failed: Top-level window has no parent.`);
            return false;
        }
    }
}
