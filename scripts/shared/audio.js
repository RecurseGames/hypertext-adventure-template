{
    function onPlayFailed(e) {
        console.warn("Unable to play audio. This may happen when accessing pages directly.", e);
    }
    
    function getAbsoluteURL(url) {
        const link = document.createElement('a');
        link.href = url;
        return link.href;
    }

    $.play = function(src) {
        if ($.IS_CHILD) {
            src = getAbsoluteURL(src);
            $._commandParent("play", src);
        } else {
            const audio = new Audio(src);
            console.log(audio.currentSrc);
            console.log(audio.baseURI);
            audio.play().catch(onPlayFailed);
        }
    }

    function getMusic() {
        return music && music.getAttribute("src");
    }

    let music = undefined;
    $.setMusic = function(src) {
        if ($.IS_CHILD) {
            src = getAbsoluteURL(src);
            $._commandParent("setMusic", src);
        } else if (getMusic() !== src) {
            if (music) {
                music.pause();
                music.remove();
            }
            if (src) {
                music = document.createElement("audio");
                music.setAttribute("loop", true);
                music.setAttribute("src", src);
                document.body.appendChild(music);
                music.play().catch(onPlayFailed);
                window.muzak = music;
            } else {
                music == undefined;
            }
        }
    }
    
    $._onCommand("play", $.play);
    $._onCommand("setMusic", $.setMusic);
    $._beforeSave(() => {
        if (music) {
            music.pause();
            music.remove();
        }
    });
    $._afterSave(() => {
        if (music) {
            document.body.appendChild(music);
            music.play();
        }
    });
}
