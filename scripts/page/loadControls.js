{
	const controlPanel = $.create("div", {
		"id": $.ID_CONTROL_PANEL,
	});
	
	function isAccessibilityMode() {
		const state = $.getState();
		if (state["_isSaving"]) {
			return false;
		} else if (state._isAccessibilityMode === undefined && $.HAS_EDITOR) {
			return true;
		} else {
			return Boolean(state._isAccessibilityMode);
		}
	}

	function toggleAccessibilityMode() {
		$.updateState({_isAccessibilityMode: !isAccessibilityMode()});
	}
	
	$.onUpdate(_ => {
		if (isAccessibilityMode()) {
			$._getNavigation().classList.add($.CLASS_BODY_ACCESSIBILITY_MODE);
		} else {
			$._getNavigation().classList.remove($.CLASS_BODY_ACCESSIBILITY_MODE);
		}
	});

	$._addControl = function(symbol, onclick, className) {
		const control = $.create("a", {
			"href": "javascript: void(0);",
		});
		control.classList.add(className);
		control.innerHTML = symbol;
		control.addEventListener("click", () => onclick());
		controlPanel.appendChild(control);
	}

	function addControlPanel() {
		document.body.appendChild(controlPanel);
	}
	
	$._addControl("&#128065;", toggleAccessibilityMode, $.CLASS_CONTROL_ICON);

	$._afterSave(addControlPanel);
	$._beforeSave(() => controlPanel.remove());
	addControlPanel();
}
