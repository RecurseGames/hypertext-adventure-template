if ($.HAS_EDITOR) {
	const DEFAULT_NAV_ELEMENT_WIDTH = "5%";
	const DEFAULT_NAV_ELEMENT_HEIGHT = "10%";

	let editedElement = null;

	function getArticle(hash) {
		return hash ? document.getElementById(hash) : document.getElementsByTagName("main")[0];
	}

	function getOrCreateArticle(hash) {
		let article = getArticle(hash);
		if (!article) {
			if (hash) {
				article = $.create("article", {"id": hash});
				document.body.appendChild(article);
				$._refreshHash();
			} else {
				article = $.create("main");
				document.body.prepend(article);
			}
		}
		return article;
	}

	function listArticles(editor) {
		let articlesWithoutId = 0;
		editor.innerHTML = "<p>Articles</p>";
		const list = editor.appendChild($.create("p"))
		list.innerHTML = "<a href=\"#\">main</a>";
		new Array(...document.getElementsByTagName("article")).forEach(article => {
			const articleId = article.id;
			if (articleId) {
				const link = $.create("a", {
					"href": `#${articleId}`,
				}, `#${articleId}`);
				list.append(", ", link);
			} else {
				console.log(article);
				articlesWithoutId++;
			}
		});
		if (articlesWithoutId) {
			list.append(`, ${articlesWithoutId} articles without id`);
		}
	}

	function updateArticle(editor) {
		const hash = $.getHash();
		const originalArticle = getOrCreateArticle(hash);

		editor.innerHTML = `<p>${hash ? "#" + hash : "main"}</p>`;
		const textField = editor.appendChild($.create("textarea", {}, originalArticle && originalArticle.innerHTML));
		textField.oninput = e => {
			if (textField.value.length == 1 && textField.value != "<") {
				textField.value = `<p>${textField.value}</p>`;
				textField.setSelectionRange(4, 4);
			} else if (textField.selectionStart == textField.selectionEnd) {
				let prefix = textField.value.substring(0, textField.selectionStart);
				let suffix = textField.value.substring(textField.selectionStart);
				if (prefix.endsWith("\n") && suffix.startsWith("</p>")) {
					prefix = prefix.substring(0, prefix.length - 1) + "</p>\n<p>";
					textField.value = prefix + suffix;
					textField.setSelectionRange(prefix.length, prefix.length);
				} else if (prefix.endsWith("<a ") && !suffix.startsWith("href=")) {
					prefix += "href=\""
					suffix = "\"></a>" + suffix;
					textField.value = prefix + suffix;
					textField.setSelectionRange(prefix.length, prefix.length);
				}
			}

			if (textField.value) {
				const article = getOrCreateArticle(hash);
				article.innerHTML = textField.value;
			} else {
				const article = getArticle(hash);
				if (article) {
					article.remove();
				}
			}

			$._setPageModified();
		};
	}

	$.onUpdate(() => {
		const hash = $.getHash();
		const article = getArticle(hash);
		if (!article || hash && article.innerHTML === "") {
			$._editTop(updateArticle);
		} else {
			$._editTop(null);
		}
	});

	function createClassButtons(element, classes, isMultipleChoice = false) {
		const buttons = [];
		classes.forEach(className => {
			let value = element.classList.contains(className);
			const button = $.create("a", {
				"class": `${$.CLASS_EDITOR_CLASS_BUTTON} ${className} ${$.CLASS_TOGGLE_BUTTON}`,
				"href": "javascript: void(0);",
			}, className);
			buttons.push(button);
			button.onclick = e => {
				$._setPageModified();
				if (!isMultipleChoice) {
					element.classList.remove(...classes);
					buttons.forEach(b => b.classList.remove($.CLASS_EDITED_ELEMENT));
				} else {
					element.classList.remove(className);
					button.classList.remove($.CLASS_EDITED_ELEMENT);
				}
				value = !value;
				if (value) {
					element.classList.add(className);
					button.classList.add($.CLASS_EDITED_ELEMENT);
				}
			};
			
			if (value) {
				button.classList.add($.CLASS_EDITED_ELEMENT);
			}
		});
		return buttons;
	}

	function setEditedElement(element) {
		if (editedElement) {
			editedElement.classList.remove($.CLASS_EDITED_ELEMENT);
		}
		
		editedElement = element;

		if (editedElement) {
			if (element !== $._getNavigation()) {
				$.updateState({_isMoveAndResizeMode: true});
			}
			
			const tagName = editedElement.tagName.toLowerCase();
			const editor = $._createEditBottom();
			editedElement.classList.add($.CLASS_EDITED_ELEMENT);
			document.body.appendChild(editor);
			editor.innerHTML = "";

			if (tagName != "nav") {
				const buttons = editor.appendChild($.create("p"));
				buttons.appendChild($.create("button", {}, "Click")).onclick = () => {
					$._removeEditBottom();
					element.click();
				};
				buttons.appendChild($.create("button", {}, "Copy")).onclick = () => {
					createLink(element.tagName, element);
				};
				buttons.appendChild($.create("button", {}, "Delete")).onclick = () => {
					element.remove();
					setEditedElement(null);
				};
			} else {
				editor.append($.create("p", {}, $.PAGE_NAME));
			}

			const indentedAttributes = {"style": "margin-left: 5%;"};
			editor.append($.create("p", {}, `<${tagName}`));
			const textFields = editor.appendChild($.create("p", indentedAttributes));
			if (tagName == "a") {
				const hrefField = $._createAttributeField(element, "href");
				const articleHashes = new Array(...document.getElementsByTagName("article"))
					.map(article => article.id)
					.filter(id => id)
					.map(id => `#${id}`);
				textFields.append(
					"href=\"",
					hrefField,
					" ",
					$._createSelectField(hrefField, articleHashes),
					"\" ",
				);
			}
			if (tagName == "img") {
				textFields.append("src=\"", $._createAttributeField(element, "src"), "\" ");
			}
			if (tagName == "nav") {
				const backgroundField = $._createField(
					$._getInitialBackground,
					$._setInitialBackground,
				);
				const defaultBackgroundButton = $.create("button", {}, "Use default");
				defaultBackgroundButton.onclick = () => {
					const defaultBackground = "images/" + $.PAGE_NAME.replace(".html", ".png");
					backgroundField.value = defaultBackground;
					$._setInitialBackground(defaultBackground, true);
					$._setPageModified();
				};
				textFields.append(
					"backgroundImageUrl=\"", 
					backgroundField,
					"\" ",
					defaultBackgroundButton,
				);
			}
			if (tagName != "nav") {
				const onclickField = $._createAttributeField(element, "onclick");
				const functions = Object.keys($)
					.filter(key => key.match(/^[a-z]/))
					.filter(key => typeof($[key]) == "function")
					.map(key => `$.${key}()`);
				textFields.append(
					"onclick=\"", 
					onclickField,
					" ",
					$._createSelectField(onclickField, functions),
					"\"",
				);
				editor.appendChild($.create("p", indentedAttributes)).append(
					"class=\"",
					...createClassButtons(element, $.EDITOR_CLASSES),
					"\"",
				);
				editor.appendChild($.create("p", indentedAttributes)).append(
					"extraClasses=\"",
					...createClassButtons(element, $.EDITOR_EXTRA_CLASSES, true),
					"\"",
				);	
			}
			editor.append($.create("p", {}, "/>"));
		} else {
			$._removeEditBottom();
		}
	}

	function createLink(tagName, template) {
		console.log(template);
		const link = $.create(tagName, {
			"class": template ? template.getAttribute("class") : "",
			"style": template
				? template.getAttribute("style") 
				: `width: ${DEFAULT_NAV_ELEMENT_WIDTH}; height: ${DEFAULT_NAV_ELEMENT_HEIGHT};`,
		});
		if (tagName.toLowerCase() == "a" && template) {
			link.setAttribute("href", template.getAttribute("href"));
		}
		if (tagName.toLowerCase() == "img" && template) {
			link.setAttribute("src", template.getAttribute("src"));
		}

		const container = $._getNavigation();
		container.appendChild(link);
		container.append("\n");
		$._setPageModified();

		setEditedElement(link);
	}

	$._getNavigation().addEventListener('mousedown', e => {
		if (isMoveAndResizeMode()) {
			if (e.target != $._getNavigation()) {
				setEditedElement(e.target);
			} else {
				setEditedElement(null);
			}
		}
	});

	function isMoveAndResizeMode() {
		const state = $.getState();
		if (state["_isSaving"]) {
			return false;
		} else if (state._isMoveAndResizeMode === undefined && $.HAS_EDITOR) {
			return true;
		} else {
			return Boolean(state._isMoveAndResizeMode);
		}
	}

	function toggleMoveAndResizeMode() {
		$.updateState({_isMoveAndResizeMode: !isMoveAndResizeMode()});
		if (!isMoveAndResizeMode()) {
			$._editTop(null);
			$._removeEditBottom();
		}
	}

	$.onUpdate(_ => {
		$._setMoveAndResizeChildren($._getNavigation(), isMoveAndResizeMode());
	});

	$._addControl("&#8690;&#xFE0E; Edit mode", toggleMoveAndResizeMode);
	$._addControl("&#x1F4BE;&#xFE0E; Save", $._save);
	$._addControl("&#x1F517;&#xFE0E; Add link", () => createLink("a"));
	$._addControl("&#x1F5BC;&#xFE0E; Add image", () => createLink("img"));
	$._addControl("&#x1F4C7;&#xFE0E; Articles", () => $._toggleEditTop(listArticles));
	$._addControl("&#x1F4DD;&#xFE0E; Edit text", () => $._toggleEditTop(updateArticle));
	$._addControl("&#x2795;&#xFE0E; New page", () => window.location.href = $.TEMPLATE_PATH);
	$._addControl("&#x1F5BB;&#xFE0E; Edit page", () => {
		setEditedElement($._getNavigation());
		$._editTop(null);
	});

	$._beforeSave(() => {
		$._editTop(null);
		$._removeEditBottom();
		setEditedElement(null);
		$._setMoveAndResizeChildren($._getNavigation(), false);
	});

	$._afterSave(() => {
		$._setMoveAndResizeChildren($._getNavigation(), isMoveAndResizeMode());
	});
}
