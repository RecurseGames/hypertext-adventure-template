{   
    const currentPath = window.location.pathname.split("/");
    $.PAGE_NAME = currentPath[currentPath.length - 1];

	$.create = function(tagName, attributes, innerText) {
		const element = document.createElement(tagName);
        if (attributes) {
            for (let attribute in attributes) {
                element.setAttribute(attribute, attributes[attribute]);
            }
        }
        if (innerText) {
            element.innerText = innerText;
        }
        return element;
	}

    $._getNavigation = function() {
        const elements = document.getElementsByTagName("nav");
        if (elements.length == 0) {
            throw new Error("Could not find <nav> element.");
        } else if (elements.length > 1) {
            throw new Error("Found multiple <nav> elements.");
        }
        return elements[0];
    }

    $.getBackground = function() {
        const background = $._getNavigation().style.backgroundImage;
        if (/url\(.*\)/.test(background)) {
            return background.substr(5, background.length - 7);
        } else {
            return "";
        }
    }

    $.setBackground = function(url) {
        const backgroundImage = url ? `url(${url})` : "";
        $._getNavigation().style.backgroundImage = backgroundImage;
    }

    let initialBackgroundImage = $.getBackground();
    $._getInitialBackground = function() {
        return initialBackgroundImage;
    }
    
    $._setInitialBackground = function(url) {
        initialBackgroundImage = url;
        $.setBackground(url);
    }

    let tempBackground;
    $._beforeSave(() => {
        tempBackground = $.getBackground();
        $.setBackground(initialBackgroundImage);
    });

    $._afterSave(() => {
        $.setBackground(tempBackground);

        // Hide elements with dynamic visibility to avoid them flashing on screen
        // when the page loads. We assume an update will fix this up later.
        for (let e of document.getElementsByClassName($.CLASS_VISIBILITY_ON)) {
            e.classList.remove($.CLASS_VISIBILITY_ON);
            e.classList.add($.CLASS_VISIBILITY_OFF);
        }
    });

    function setVisibility(element, isVisible) {
        if (element) {
            if (isVisible) {
                element.classList.add($.CLASS_VISIBILITY_ON);
                element.classList.remove($.CLASS_VISIBILITY_OFF);
            } else {
                element.classList.remove($.CLASS_VISIBILITY_ON);
                element.classList.add($.CLASS_VISIBILITY_OFF);
            }
        }
    }

    $.setClassVisibility = function(className, isVisible) {
        for (let element of document.getElementsByClassName(className)) {
            setVisibility(element, isVisible);
        }
    }

    $.setVisibility = function(id, isVisible) {
        const element = document.getElementById(id);
        if (element) {
            if (isVisible) {
                element.classList.add($.CLASS_VISIBILITY_ON);
                element.classList.remove($.CLASS_VISIBILITY_OFF);
            } else {
                element.classList.remove($.CLASS_VISIBILITY_ON);
                element.classList.add($.CLASS_VISIBILITY_OFF);
            }
        }
    }
}
