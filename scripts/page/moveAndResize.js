{
	let draggedElement = null;
	
	function cancel(e) {
		if (draggedElement) {
			draggedElement.classList.remove($.CLASS_BODY_DRAG_MODE);
		}
		draggedElement = null;
	}

	function mousedown(e) {
		const parent = e.currentTarget;
		if (e.target != parent) {
			draggedElement = e.target;

			// Prevent default dragging behaviours
			e.preventDefault();
			draggedElement.classList.add($.CLASS_BODY_DRAG_MODE);
		}
	}

	function mousemove(e) {
		const parent = e.currentTarget;
		if (draggedElement) {
			if (e.shiftKey) {
				const outlineSize = 2;
				const maxWidth = parent.offsetWidth - draggedElement.offsetLeft;
				const maxHeight = parent.offsetHeight - draggedElement.offsetTop;
				const newWidth = Math.max(0, Math.min(maxWidth, draggedElement.offsetWidth + e.movementX - outlineSize));
				const newHeight = Math.max(0, Math.min(maxHeight, draggedElement.offsetHeight + e.movementY - outlineSize));

				draggedElement.style.width = `${(newWidth / parent.offsetWidth * 100).toFixed(2)}%`;
				draggedElement.style.height = `${(newHeight / parent.offsetHeight * 100).toFixed(2)}%`;
			} else {
				const maxLeft = parent.offsetWidth - draggedElement.offsetWidth;
				const maxTop = parent.offsetHeight - draggedElement.offsetHeight;
				const newLeft = Math.max(0, Math.min(maxLeft, draggedElement.offsetLeft + e.movementX));
				const newTop = Math.max(0, Math.min(maxTop, draggedElement.offsetTop + e.movementY));

				draggedElement.style.left = `${(newLeft / parent.offsetWidth * 100).toFixed(2)}%`;
				draggedElement.style.top = `${(newTop / parent.offsetHeight * 100).toFixed(2)}%`;
			}

			$._setPageModified();
		}
	}

	$._setMoveAndResizeChildren = function(parent, isEnabled) {
		if (isEnabled) {
			parent.classList.add($.CLASS_MOVE_AND_RESIZE_AREA);
			parent.addEventListener('mouseleave', cancel);
			parent.addEventListener('mouseup', cancel);
			parent.addEventListener('mousedown', mousedown);
			parent.addEventListener('mousemove', mousemove);
		} else {
			parent.classList.remove($.CLASS_MOVE_AND_RESIZE_AREA);
			parent.removeEventListener('mouseleave', cancel);
			parent.removeEventListener('mouseup', cancel);
			parent.removeEventListener('mousedown', mousedown);
			parent.removeEventListener('mousemove', mousemove);
		}
	}
}
