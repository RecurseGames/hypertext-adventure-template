$.ID_CONTROL_PANEL = "controlPanel";
$.ID_EDITOR_TOP = "editorTop";
$.ID_EDITOR_BOTTOM = "editorBottom";

$.CLASS_TOGGLE_BUTTON = "toggleButton";
$.CLASS_EDITED_ELEMENT = "editedLink";
$.CLASS_CONTROL_ICON = "controlIcon";
$.CLASS_MOVE_AND_RESIZE_AREA = "moveAndResizeArea";
$.CLASS_BODY_DRAG_MODE = "inDragMode";
$.CLASS_BODY_ACCESSIBILITY_MODE = "inAccessibilityMode";
$.CLASS_VISIBILITY_OFF = "isHidden";
$.CLASS_VISIBILITY_ON = "isVisible";
