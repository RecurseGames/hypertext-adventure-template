if ($.HAS_EDITOR) {
	const editorTop = $.create("div", {
		"id": $.ID_EDITOR_TOP,
	});

	const editorBottom = $.create("div", {
		"id": $.ID_EDITOR_BOTTOM,
	});

	let editorTopSetup = null;
	$._editTop = function(setupEditor) {
		if (!setupEditor) {
			editorTopSetup = null;
			editorTop.remove();
		} else {
			editorTopSetup = setupEditor;
			editorTop.innerHTML = "";
			document.body.appendChild(editorTop);
			setupEditor(editorTop);
		}
	}

	$._toggleEditTop = function(setupEditor) {
		if (editorTopSetup == setupEditor) {
			$._editTop(null);
		} else {
			$._editTop(setupEditor);
		}
	}

	$._createEditBottom = function() {
		editorBottom.innerHTML = "";
		document.body.appendChild(editorBottom);
		return editorBottom;
	}
	
	$._removeEditBottom = function() {
		editorBottom.remove();
	}

	$._createField = function(get, set) {
		const field = $.create("input", {
			"type": "text",
			"value": get(),
		});
		field.oninput = e => {
			const value = e.target.value;
			$._setPageModified();
			set(value);
		};
		return field;
	}

	$._createAttributeField = function(element, attributeName) {
		return $._createField(
			() => element.getAttribute(attributeName) || "",
			value => {
				if (value) {
					element.setAttribute(attributeName, value);
				} else {
					element.removeAttribute(attributeName);
				}
			},
		);
	}

	$._createSelectField = function(input, values) {
		const field = $.create("select");
		field.append(...["", ...values].map(value => $.create(
			"option",
			{"value": value},
			value,
		)));
		field.onchange = e => {
			$._setPageModified();
			input.value = e.target.value;
			e.target.value = undefined;
		};
		return field;
	}
}
