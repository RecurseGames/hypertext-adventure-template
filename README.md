# Hypertext Adventure Template (HTAT)

This is a free template for making simple point-and-click adventure games using webpages. No coding skills are needed, and they can be easily uploaded to [itch.io](https://itch.io/). I made my own game with this template, [Look Inside Some Guy's House](https://recursegames.itch.io/look-inside-some-guys-house). If you make anything yourself, let me know!

This template is early in development, so apologies if anything is a bit flakey. I may update it from time to time.

## Getting Started

To create a new game, simply copy-and-paste the template and open *index.html* in your browser. You will see the initial page, *pages/special/splash_screen.html*, with a link to a short tutorial.

The editor provides the following buttons:

- **Edit mode** shows and hides highlighting of clickable areas, which can be moved and resized (while holding *shift*) with the mouse.
  - When you click a highlighted area, you will see options for editing it. **Click** will click it as if edit mode is off (potentially navigating to a new page). You can also **Copy** and **Delete** it, and specify JavaScript to run when it's clicked.
  - When you're editing a link, you can specify its `href` attribute (i.e., where it goes). If you don't know HTML, see [Tips for Beginners](#tips-for-beginners) below.
- **Save** saves the current page. Pages are just HTML files, and can be edited manually. They should be kept in a subfolder of the *pages* folder (e.g., *pages/special/splash_page.html*).
  - Pages include a `<script>` element for loading the JavaScript in *scripts.page.js*. If you change the folder structure, you may need to update the page's path to this file.
  - Save frequently, as this template is fairly crude and may occasionally crash (particular in Chrome when clicking the back button, see [](https://bugs.chromium.org/p/chromium/issues/detail?id=1038223)).
- **Add link** and **Add image** add clickable links and images to the page.
- **Articles** lists all pieces of text on the page. The default text is called *main*, but others have hashtags (e.g., *#example*). Adding the hashtag to a page's URL will show that text (e.g., *index.html#example*). HTML links to different texts look like`<a href="#example">Example</a>` and `<a href="">Main</a>`.
- **Edit text** shows an editor for the currently displayed text. The editor accepts HTML, and automatically inserts paragraphs and links as you type. If you don't know HTML, see [Tips for Beginners](#tips-for-beginners) below.
- **New page** creates a new page from a template. You will be prompted if the current page has unsaved changes first.
- **Edit page** let's you specify the page's background image. The default image location is based on the page name (e.g., *index.png* for *index.html*).

When you're ready to publish your game, simply disable the editor by specifying `$.HAS_EDITOR = false;` in *configuration.js*.

## Tips for Beginners

The game is made of webpages, which are just HTML files. You can edit them manually if you're familiar with HTML, but most of the game's features can be used in the editor.

When editing text, you can insert a link by typing something like `<a href="#example">Click here</a>`. This is HTML code, and will be displayed as a link on the page. Note that `<` and `>` has special meaning in HTML, and can't be added to text normally. If you want to display `<` or `>` on the page, you'll need to type `&lt;` or `&gt;`. The `href` value (in this case `#example`) specifies where the link goes. Common values are:

1. `#` goes to the current page, with the default text displayed.
2. `#example` goes to the current page, with alternative text displayed. `#example` is the name of the alternative text, and these names can be listed with the editor's **Articles** button. To make new text, simply make a link with a new hashtag and click it - the editor will immediately allow this text to be edited.
3. `page.html` goes to another page in the same folder as the current page (in this case, the page *page.html*).
4. `../folder/page.html` goes out of the folder which contains the current page, and into a page from another folder.

The HTML code for an image is something like `<img src="image.png"></img>`. The `src` specifies the image file, and works similarly to `href`.

When editing a link or image in the editor, you will see a control for changing its `href` or `src`. You can provide the same values here as if you were writing HTML.

## Scripting

More advanced users can specify JavaScript to be run when links are clicked. The template provides a number of useful JavaScript functions, including:

1. `$.setMusic` plays music (e.g., `$.setMusic('../clock.mp3');`).

2. `$.play` plays sound effects (e.g., `$.play('../bell.mp3');`).

3. `$.setBackground` changes the page background. This will be reset when the page loads. For example, `$.setBackground("sky_night.png")`.

4. `$.setVisibility` changes the visibility of page elements, identified by ID. This will be reset when the page loads. For example, `$.setVisibility('secret_message', true)`.

5. `$.setClassVisibility` changes the visibility of all page elements with a given class.

6. `$.getHash` gets the hashcode of the currently displayed text (e.g, *#example*), or an empty string if the default text is displayed. You can also change the currently displayed text with `$.setHash`.

7. `$.getState` and `$.updateState` are used to get and update data. You can also specify functions to be run whenever data changes with `$.onUpdate`. For example, this code will show a secret message when the page loads:

   ```js
   $.onUpdate((hash, state) => {
     if (state.showSecretMessage) {
       $.setVisibility('secret_message', true);
     }
   });
   $.updateState({'showSecretMessage': true});
   ```

8. `$.onLoad` specifies a function should be called once, after the page has loaded (e.g., `$.onLoad(() => alert('Page loaded'))`).  Many other functions on `$` will not work before the page has loaded, and should only be called from `onclick` handlers or functions passed to `$.onLoad` or `$.onUpdate`.

## Customisation

*configuration.js* is used to customise the editor. When you're ready to publish your game, you can disable the editor by specifying `$.HAS_EDITOR = false;` in this file. You can also specify the location of the template page.

You can customise the appearance of your game by editing the files in the *theme* folder and specifying the editor's CSS classes in *configuration.js*. You can also edit the various JavaScript files in *scripts* to add new features. However this requires skill with CSS and JavaScript - you'll have to figure it out yourself.

## Technical Details

The game is played in an `<iframe>` at *index.html*. This allows *index.html* to continue playing sound and music when the player navigates to different pages.

Each page includes the HTML `<script src="../../scripts/page.js"></script>`, which loads *page.js*. This will then load additional scripts, used to set up the editor and JavaScript functions usable by the page.