/* Configuration */

$.HAS_EDITOR = true;

/* Editor Configuration */

$.TEMPLATE_PATH = "../special/template.html";
$.EDITOR_CLASSES = ["n", "ne", "e", "se", "s", "sw", "w", "nw", "look"];
$.EDITOR_EXTRA_CLASSES = ["sendToFront", "sendToBack", "postcard"];
